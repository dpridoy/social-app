import React from 'react'
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'

const Status=({statusResponse})=> {
    return (
        // <div>
        //     <h3>{statusResponse.gsipk}</h3>
        //     <h4>{statusResponse.status}</h4>
        //     <h6>{statusResponse.datetime}</h6>
        // </div>

       <div className='container' >
            <h4>{statusResponse.gsipk.substring(8)}</h4>
            <h6>{statusResponse.datetime}</h6>
            <p>{statusResponse.status}</p>
            
       </div>

        
    )
}

export default Status
