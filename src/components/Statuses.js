import React from 'react'
import Status from './Status'

const Statuses = ({statusResponses}) => {
    return (
        <>
            {statusResponses.map((statusResponse,index)=>(
                <Status key={index} statusResponse={statusResponse}></Status>
            ))}
        </>
    )
}

export default Statuses
