import React from 'react'
import { useState } from 'react'

const AddStatus = ({onAdd}) => {
    const [text, setText] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()
        if(!text){
            alert('Please add a status')
            return
        }

        onAdd({text})
        setText('')
    }

    return (
        <form className='add-form' onSubmit={onSubmit}>
            <div className='container' >
                <div className="mb-3">
                    <label  className="form-label">Enter your status</label>
                    <textarea className="form-control" rows="3"
                    value={text} onChange={(e)=>setText(e.target.value)}></textarea>
                    <br></br>
                    <input style={{backgroundColor: '#f2f2f2'}} className='btn' type='submit' value='Publish'></input>
                </div>
            </div>
        </form>
    )
}

export default AddStatus
