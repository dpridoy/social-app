import '../App.css';
import React , { useState } from 'react'

async function loginUser(credentials) {
    
    return fetch('https://249w7bgaoc.execute-api.us-east-2.amazonaws.com/dev/login', {
      method: 'POST',  
      headers: {
        'Content-Type': 'application/json',
      },
      body : JSON.stringify(credentials),
    })
    .then(data => data.json())
}

const Login = ({setResponse}) => {
    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = async e => {
        e.preventDefault();
        if(!username){
            alert('Username required!')
            return
        }
        if(!password){
            alert('Password required!')
            return
        }
        const res = await loginUser({
          username,
          password
        });
        console.log(res)
        if(!res['message']){
            setResponse(res);
        }
        else{
            alert('Wrong credentials!!')
        }
      }

    return (
        <div className="container App">
            <h1>Log In</h1>
            <form onSubmit={handleSubmit}>
                <label>
                <p>Username</p>
                <input type="text" value={username} onChange={e => setUserName(e.target.value)} />
                </label><br></br>
                <label>
                <p>Password</p>
                <input type="password" value={password} onChange={e => setPassword(e.target.value)}/>
                </label><br></br>
                <div>
                <button style={{backgroundColor: '#f2f2f2'}} className='btn' type="submit">Login</button>
                </div>
            </form>
        </div>
    )
}

export default Login
