import React, { useState, useEffect } from 'react'
import {Link, useHistory} from 'react-router-dom'
import Statuses from './Statuses'
import useToken from './useToken'
import AddStatus from './AddStatus'

const Dashboard = () => {
    const [statuses, setStatuses] = useState([])
    const {token, setToken} = useToken();

    useEffect(()=>{
        const getStatuses = async()=>{
            const statusesResponse = await fetchStatuses()
            setStatuses(statusesResponse)
        }
        getStatuses()
    },[])

    const history = useHistory()
    const logOut=()=>{
        sessionStorage.clear()
        history.push('/')
        refreshPage()
    }

    const fetchStatuses= async()=>{
        const res = await fetch('https://249w7bgaoc.execute-api.us-east-2.amazonaws.com/dev/status')
        const data = await res.json()
        return data
    }

    const refreshPage = ()=>{
        window.location.reload();
     }

    const addStatus = async (status)=>{
        
        const res = await fetch('https://249w7bgaoc.execute-api.us-east-2.amazonaws.com/dev/status',{
            method: 'POST',
            headers:{
                'Content-type': 'application/json'
            },
            body: JSON.stringify({'sk':token['sk'], 'username': token['username'],
            "gsisk" : token['gsisk'],
            "status" : status['text']})
        })

        const data = await res.json()
        refreshPage()
        // setStatuses([...statuses,data])
    }

    return (
        <div>
            <h2>Dashboard</h2>
            <button style={{backgroundColor: '#f2f2f2'}} className='btn' onClick={logOut} >Logout</button>
            <AddStatus onAdd={addStatus}></AddStatus>
            <Statuses statusResponses={statuses}></Statuses>
            
        </div>
    )
}

export default Dashboard
