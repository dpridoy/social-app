import './App.css';
import React, {useState} from 'react';
import {BrowserRouter as Router , Route, Switch } from 'react-router-dom'
import Dashboard from './components/Dashboard';
import Login from './components/Login';
import useToken from './components/useToken';


// function setResponse(userResponse){
//   sessionStorage.setItem('user', JSON.stringify(userResponse));
// }

// function getResponse(){
//   const tokenString = sessionStorage.getItem('user');
//   // console.log(tokenString)
//   const userToken = JSON.parse(tokenString);
//   // console.log(userToken);
//   return userToken
// }


function App() {
  const {token, setToken} = useToken();
  console.log('TOKEN')
  console.log(token)
  if(!token) {
    return <Login setResponse={setToken}/>
  }

  return (
    <div className="App">
      <h1>Social App</h1>
      <Router>
        <Switch>
          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
